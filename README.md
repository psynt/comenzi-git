# Git commands
## Initialisation

#### If the remote is empty (contains no files)
** only `init` once per project! **  
`git init`  
`git remote add origin *url*`

#### If the remote is not empty
download a project that has already been started.  
`git clone *url* *folder_name*`


## Useful commands

`git status`

`git add .`

`git commit -m "*message*"`  
*e.g. git commit -m "suicide"*

`git push` (+ `-u origin *branch_name*`). The -u and after part only needs to be used oonce per (local) branch.  
*e.g. git push -u origin master*

Use `git push remote_name :branch_name` to delete a **remote** branch. If in doubt, ask what this means.

`git checkout` the new name of the tardis. The command to travel through hisotry.  
*Avoid using `git checkout` on a dirty tree!*  

`git merge` to... merge (branches)  

`git branch` to view / create / delete branches  

## Multi-player

**DON'T REWRITE PUSHED HISTORY.** Everyone will just hate you.

If you're not allowed to push, it usually means that someone else pushed after you last pulled. (=fetch+merge)

`git pull` to... pull changes. If you're not allowed to push, you will have to first pull, and **merge**.

## Merge

In 99/100 cases, git merges automatically.  
Evidently, you will only ever run into that 1 case. If you do, git will insert those weird chevron type signs in your file to show the bits that have diverged.  
Choose how to deal with the crisis, remove those markings and commit the merge resolution.  


Merge process, let's say you want to merge your branch named **branch_a** into another branch, let's say **branch_b**

`git checkout branch_b` -- go to the branch that you want to merge your branch to

`git merge branch_a` -- merge your *branch_b* into branch2

